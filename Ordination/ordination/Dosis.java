package ordination;

import java.time.LocalTime;
import java.util.Objects;

public class Dosis {
    private LocalTime tid;
    private double antal;

    public Dosis(LocalTime tid, double antal) {
        super();
        this.tid = tid;
        this.antal = antal;
    }

    public double getAntal() {
        return antal;
    }

    public void setAntal(double antal) {
        this.antal = antal;
    }

    public LocalTime getTid() {
        return tid;
    }

    public void setTid(LocalTime tid) {
        this.tid = tid;
    }

    @Override
    public String toString(){
        return "Kl: " + tid + "   antal:  " + antal;
    }

    @Override
    public boolean equals(Object o) {
        try {
            Dosis d = (Dosis) o;
            if (d.getTid() == this.getTid() && d.getAntal() == this.getAntal()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
