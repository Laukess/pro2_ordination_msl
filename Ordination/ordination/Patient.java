package ordination;

import java.util.ArrayList;

public class Patient {
    private String cprnr;
    private String navn;
    private double vaegt;
    private ArrayList<Ordination> ordi;

    public Patient(String cprnr, String navn, double vaegt) {
        this.cprnr = cprnr;
        this.navn = navn;
        this.vaegt = vaegt;
        ordi = new ArrayList<Ordination>();
    }

    public String getCprnr() {
        return cprnr;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getVaegt(){
        return vaegt;
    }

    public void setVaegt(double vaegt){
        this.vaegt = vaegt;
    }




    @Override
    public String toString(){
        return navn + "  " + cprnr;
    }

    //TODO: Metoder (med specifikation) til at vedligeholde link til Ordination
    public ArrayList<Ordination> getOrdinationer() {
        return new ArrayList<>(ordi);
    }

    public void addOrdination(Ordination ord1)
    {
        ordi.add(ord1);
    }

    public void removeOrdination(Ordination ord1)
    {
        ordi.remove(ord1);
    }

}
