package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

    ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDen,
                       LocalDate slutDen,
                       LocalTime[] klokkeSlet, double[] antalEnheder) {
        super(startDen, slutDen);

        for (int i = 0 ; i < klokkeSlet.length ; i++) {
            opretDosis(klokkeSlet[i], antalEnheder[i]);
        }
    }

    public ArrayList<Dosis> getDoser() {
        return this.doser;
    }

    public void opretDosis(LocalTime tid, double antal) {
        doser.add(new Dosis(tid, antal));
    }


    public double samletDosis() {
        return this.antalDage() * doegnDosis();
    }


    public double doegnDosis() {
        double sum = 0;
        for (Dosis d : doser) {
            sum += d.getAntal();
        }
        return sum;
    }


    public String getType() {
        return "Daglig skaev";
    }
}
