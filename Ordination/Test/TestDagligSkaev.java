package Test;

import static org.junit.Assert.*;

import ordination.DagligSkaev;
import ordination.Dosis;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class TestDagligSkaev {

	public DagligSkaev ds;

	@Before
	public void setUp() throws Exception {
		LocalTime lda[] = new LocalTime[2];
		lda[0] = LocalTime.of(12, 0);
		lda[1] = LocalTime.of(18, 0);
		double enheder[] = new double[2];
		enheder[0] = 1;
		enheder[1] = 2;

		ds = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), lda, enheder);
	}

	@Test
	public void testSamletDosis() {
		assertEquals(3 * ds.antalDage(), ds.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(3.0, ds.doegnDosis(), 0.001);
	}

	@Test
	public void testGetType() {
		assertEquals("Daglig skaev", ds.getType());
	}

	@Test
	public void testDagligSkaev() {
		assertEquals(ds.getDoser().size(), 2);
	}

	@Test
	public void testGetDoser() {
		// implementer equials i dosis klassen!
		ArrayList<Dosis> d = new ArrayList<>();
		d.add(new Dosis(LocalTime.of(12, 0), 1));
		d.add(new Dosis(LocalTime.of(18, 0), 2));

		assertEquals(d, ds.getDoser());
	}

	@Test
	public void testOpretDosis() {
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), new LocalTime[0], new double[0]);
		Dosis d = new Dosis(LocalTime.of(12, 0), 10);
		ds1.opretDosis(LocalTime.of(12, 0), 10);

		assertEquals(d, ds1.getDoser().get(0));
	}

}
