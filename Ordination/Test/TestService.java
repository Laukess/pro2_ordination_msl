package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class TestService {
	public Patient testPatientJanne;
	public Laegemiddel testLaegemiddelAce;
	

	@Before
	public void setUp() throws Exception 
	{	
		testPatientJanne = service.Service.getService().opretPatient("Jane Jensen", "121256-0512", 63.4);
		testLaegemiddelAce = service.Service.getService().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		
	}

	@Test
	public void testOpretPNOrdinationAntal1() 
	{
		assertEquals(1, service.Service.getService().opretPNOrdination(LocalDate.now(), 
				LocalDate.now().plusDays(5), testPatientJanne, 
				testLaegemiddelAce, 1).getAntalEnheder(),0.001);
	}
	
	@Test
	public void testOpretPNOrdinationAntal1_medOrdinering() 
	{
		PN tmpPN = service.Service.getService().opretPNOrdination(LocalDate.now(), 
				LocalDate.now().plusDays(5), testPatientJanne, 
				testLaegemiddelAce, 1);

		tmpPN.givDosis(LocalDate.now().plusDays(1));
		tmpPN.givDosis(LocalDate.now().plusDays(1));
			
		assertEquals(2,tmpPN.getAntalGangeGivet() ,0.001);
	}

	@Ignore ("Der kan godt oprettes en ordination med null på patient og laegemiddel")
	@Test(expected = NullPointerException.class)
	public void testOpretPNOrdinationNull() 
	{
		service.Service.getService().opretPNOrdination(LocalDate.now(), 
				LocalDate.now().plusDays(5), null, 
				null, 0);
	}

//	@Test
//	public void testOpretDagligFastOrdinationLH() {
//		DagligFast o = service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne, testLaegemiddelAce, 0, 0, 0, 0);
//		assertEquals(o.getDoser()[0].getAntal(), 0, 0.001);
//	}
	
	@Ignore("Når alle doser er 0, giver ordinationen ikke mening")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination(){
		assertEquals(new DagligFast(LocalDate.now(), LocalDate.now().plusDays(5), 1, 1, 1, 1), 
				service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 0, 0, 0, 0));
	}
	
	@Test
	public void testOpretDagligFastOrdination1(){
		assertEquals(1.0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, 1, 1).getDoser()[0].getAntal(),0.001);
		assertEquals(1.0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, 1, 1).getDoser()[1].getAntal(),0.001);
		assertEquals(1.0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, 1, 1).getDoser()[2].getAntal(),0.001);
		assertEquals(1.0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, 1, 1).getDoser()[3].getAntal(),0.001);
	}
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination2(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 0, 0, 0, -1));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination3(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 0, 0, -1, 0));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination4(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 0, -1, 0, 0));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination5(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, -1, 0, 0, 0));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination6(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, 1, -1));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination7(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, 1, -1, 1));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination8(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, 1, -1, 1, 1));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination9(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientJanne,
				testLaegemiddelAce, -1, 1, 1, 1));
	}
	
	@Ignore("Enhver negativ dosis burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination10(){
		assertEquals(0,service.Service.getService().opretDagligFastOrdination(LocalDate.now().plusDays(5), LocalDate.now(), testPatientJanne,
				testLaegemiddelAce, -1, 1, 1, 1));
	}

	@Ignore("Enhver negativ antalEnheder burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationNegativEnheder() {
		
		LocalTime[] testKlokkeslet = {LocalTime.of(10, 00),LocalTime.of(12, 00)
										,LocalTime.of(14, 00), LocalTime.of(18, 00)};
		double[] testAntalEnheder = {-1.0, -1.0, -1.0, -1.0};
		
		service.Service.getService().opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(5)
				, testPatientJanne, testLaegemiddelAce, testKlokkeslet, testAntalEnheder);
		
	}
	
	@Ignore("antalEnheder 0 på alle burde throw en exception")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationZeroEnheder() {
		
		LocalTime[] testKlokkeslet = {LocalTime.of(10, 00),LocalTime.of(12, 00)
										,LocalTime.of(14, 00), LocalTime.of(18, 00)};
		double[] testAntalEnheder = {0.0, 0.0, 0.0, 0.0};
		
		service.Service.getService().opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(5)
				, testPatientJanne, testLaegemiddelAce, testKlokkeslet, testAntalEnheder);
		
	}
	
	@Test
	public void testOpretDagligSkaevOrdination1Enheder() {
		
		LocalTime[] testKlokkeslet = {LocalTime.of(10, 00),LocalTime.of(12, 00)
										,LocalTime.of(14, 00), LocalTime.of(18, 00)};
		double[] testAntalEnheder = {1.0, 1.0, 1.0, 1.0};
		
		DagligSkaev tmpSkaev = service.Service.getService().opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(5)
				, testPatientJanne, testLaegemiddelAce, testKlokkeslet, testAntalEnheder); 
		
		assertSame(testPatientJanne.getOrdinationer().get(0), tmpSkaev);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationIllegal() {
		
		LocalTime[] testKlokkeslet = {LocalTime.of(10, 00),LocalTime.of(12, 00)
										,LocalTime.of(14, 00), LocalTime.of(18, 00)};
		double[] testAntalEnheder = {1.0, 1.0, 1.0, 1.0};
		
		service.Service.getService().opretDagligSkaevOrdination(LocalDate.now().plusDays(5), LocalDate.now()
				, testPatientJanne, testLaegemiddelAce, testKlokkeslet, testAntalEnheder);
		
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendt() {
		
		service.Service.getService().opretPNOrdination(LocalDate.now().plusDays(5), 
				LocalDate.now(), testPatientJanne, 
				testLaegemiddelAce, 1);
		
		
	}
	
	@Ignore("En patient burde ikke kunne have en negativ vægt")
	@Test(expected = IllegalArgumentException.class)
	public void testAnbefaletDosisPrDoegnNegativeWeight() {
		Patient testPatientSvend = service.Service.getService().opretPatient("TestSvend Svendsen", "121256-0512", -1.0);
		assertEquals(-0.1, service.Service.getService().anbefaletDosisPrDoegn(testPatientSvend, testLaegemiddelAce),0.0001);
		
	}
	
	@Ignore("En patient burde ikke kunne veje 0kg")
	@Test(expected = IllegalArgumentException.class)
	public void testAnbefaletDosisPrDoegnZeroWeight() {
		Patient testPatientSvend = service.Service.getService().opretPatient("TestSvend Svendsen", "121256-0512", 0.0);
		assertEquals(0.0, service.Service.getService().anbefaletDosisPrDoegn(testPatientSvend, testLaegemiddelAce),0.0001);
		
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() 
	{
		Patient testPatientSvend = service.Service.getService().opretPatient("TestSvend Svendsen", "121256-0512", 13.0);
		Patient testPatientSvend1 = service.Service.getService().opretPatient("TestSvend1 Svendsen", "121256-0512", 17.0);
		Patient testPatientSvend2 = service.Service.getService().opretPatient("TestSvend2 Svendsen", "121256-0512", 20.0);
		
		service.Service.getService().opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientSvend, 
				testLaegemiddelAce,2, -1, 1, -1);
		
		LocalTime[] testKlokkeslet = {LocalTime.of(10, 00),LocalTime.of(12, 00)
				,LocalTime.of(14, 00), LocalTime.of(18, 00)};
		
		double[] testAntalEnheder = {1.0, 1.0, 1.0, 1.0};
		
		service.Service.getService().opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientSvend1,
				testLaegemiddelAce, testKlokkeslet, testAntalEnheder);
		
		service.Service.getService().opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(5), testPatientSvend2, testLaegemiddelAce, 15.0);
		
		assertEquals(3.0, service.Service.getService().antalOrdinationerPrVægtPrLægemiddel(0, 24, testLaegemiddelAce),0.00001);
		
	}

	@Ignore("Man burde ikke kunne oprette en patient uden navn og CPR-nr")
	@Test(expected = NullPointerException.class)
	public void testOpretPatientNull() 
	{
		service.Service.getService().opretPatient(null, null, 0);
	}
	
	@Test
	public void testOpretPatient() 
	{
		Patient testdut = service.Service.getService().opretPatient("TestSvend Svendsen", "121256-0512", 13.0);
		assertNotNull(testdut);
	}
	

	@Test
	public void testOpretLaegemiddel() {
		Laegemiddel tmpMiddel = service.Service.getService().opretLaegemiddel("testanol", 1, 1, 1, "pille");
		
		assertEquals(1.0, tmpMiddel.getEnhedPrKgPrDoegnLet(),0.001);
		assertEquals(1.0, tmpMiddel.getEnhedPrKgPrDoegnNormal(),0.001);
		assertEquals(1.0, tmpMiddel.getEnhedPrKgPrDoegnTung(),0.001);
		
		
	}
	@Ignore("Det burde ikke være mulig med negative enheder")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretLaegemiddel1() {
		Laegemiddel tmpMiddel = service.Service.getService().opretLaegemiddel("testanol", -1, -1, -1, "pille");
		
		assertEquals(-1.0, tmpMiddel.getEnhedPrKgPrDoegnLet(),0.001);
		assertEquals(-1.0, tmpMiddel.getEnhedPrKgPrDoegnNormal(),0.001);
		assertEquals(-1.0, tmpMiddel.getEnhedPrKgPrDoegnTung(),0.001);
	}
	
	@Ignore("Det burde ikke være mulig med ingen enheder")
	@Test(expected = IllegalArgumentException.class)
	public void testOpretLaegemiddel2() {
	Laegemiddel tmpMiddel = service.Service.getService().opretLaegemiddel("testanol", 0, 0, 0, "pille");
		
		assertEquals(0.0, tmpMiddel.getEnhedPrKgPrDoegnLet(),0.001);
		assertEquals(0.0, tmpMiddel.getEnhedPrKgPrDoegnNormal(),0.001);
		assertEquals(0.0, tmpMiddel.getEnhedPrKgPrDoegnTung(),0.001);
		
		
	}

}
