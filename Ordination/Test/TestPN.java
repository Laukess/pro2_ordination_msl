package Test;

import static org.junit.Assert.*;

import ordination.PN;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

public class TestPN {

	PN pn;

	@Before
	public void setUp() throws Exception {
		pn = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7));
	}

	@Test
	public void testSamletDosis() {
		assertEquals(pn.doegnDosis() * pn.antalDage(), pn.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		pn.setAntalEnheder(15);
		pn.givDosis(LocalDate.of(2016, 1, 2));
		assertEquals(15.0 / pn.antalDage(), pn.doegnDosis(), 0.001);
	}

	@Test
	public void testGetType() {
		assertEquals("PN", pn.getType());
	}

	@Test
	public void testPN() {
		PN pn2 = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7));
		assertEquals(0, pn2.getAntalEnheder(), 0.001);
		assertEquals(0, pn2.getAntalGangeGivet());
	}

	@Test
	public void testGivDosis() {
		PN pn2 = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7));
		pn2.setAntalEnheder(5);
		assertTrue(pn2.givDosis(LocalDate.of(2016, 1, 3)));
		assertEquals(pn2.getAntalGangeGivet(), 1);
	}

	@Test
	public void testGetAntalGangeGivet() {
		PN pn2 = new PN(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7));
		pn2.setAntalEnheder(5);
		assertEquals(pn2.getAntalGangeGivet(), 0);
		pn2.givDosis(LocalDate.of(2016, 1, 3));
		assertEquals(pn2.getAntalGangeGivet(), 1);
	}

	@Test
	public void testGetAntalEnheder() {
		pn.setAntalEnheder(40);
		assertEquals(40, pn.getAntalEnheder(), 0.001);
	}

	@Test
	// den kan vel godt slettes
	public void testSetAntalEnheder() {
		pn.setAntalEnheder(40);
		assertEquals(40, pn.getAntalEnheder(), 0.001);
	}

}
