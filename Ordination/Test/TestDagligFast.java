package Test;

import static org.junit.Assert.*;

import ordination.DagligFast;
import ordination.Dosis;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

public class TestDagligFast {

	DagligFast df = new DagligFast(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), 1, 2, 3, 4);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSamletDosis() {
		assertEquals(70.0, df.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(10.0, df.doegnDosis(), 0.001);
	}

	@Test
	public void testGetType() {
		assertEquals("Daglig Fast", df.getType());
	}

	@Test
	public void testDagligFast() {
		// tester om doserne er initialized ordentilgt i constructoren
		DagligFast df2 = new DagligFast(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), 4, 5, 6, 7);
		assertEquals(df2.getDoser()[0].getAntal(), 4, 0.001);
		assertEquals(df2.getDoser()[1].getAntal(), 5, 0.001);
		assertEquals(df2.getDoser()[2].getAntal(), 6, 0.001);
		assertEquals(df2.getDoser()[3].getAntal(), 7, 0.001);
	}

	@Test
	public void testOpretDosis() {
		// Whitebox, men ved ikke hvordan funktionen ellers skal testes
		df.getDoser()[0] = df.opretDosis(LocalTime.of(6, 0), 100);
		assertEquals(df.getDoser()[0].getAntal(), 100, 0.001);
		assertEquals(df.getDoser()[0].getTid(), LocalTime.of(6,0));
	}

	@Test
	public void testGetDoser() {
		// Doser skal implementere equals.
		Dosis da[] = new Dosis[4];
		DagligFast df3 = new DagligFast(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 7), 2, 4, 6, 8);
		da[0] = new Dosis(LocalTime.of(6,0), 2);
		da[1] = new Dosis(LocalTime.of(12,0), 4);
		da[2] = new Dosis(LocalTime.of(18,0), 6);
		da[3] = new Dosis(LocalTime.of(0,0), 8);
		assertArrayEquals(da, df3.getDoser());
//		assertArrayEquals(new int[]{1,2}, new int[]{1,2});
	}

}
